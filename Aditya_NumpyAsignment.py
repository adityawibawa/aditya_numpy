#!/usr/bin/env python
# coding: utf-8

# In[49]:


#Import NumPy as np
import numpy as np


# In[50]:


#Create an array of 10 zeros
np.zeros(10)


# In[51]:


#Create an array of 10 ones
np.ones(10)


# In[52]:


#Create an array of 10 fives
np.ones(10)*5


# In[53]:


#Create an array of the integers from 10 to 50
np.arange(10,51)


# In[54]:


#Create an array of all the even integers from 10 to 50
np.arange(10,51,2)


# In[55]:


#Create a 3x3 matrix with values ranging from 0 to 8
arr = np.arange(0,9)
arr.reshape((3,3))


# In[56]:


#Create a 3x3 identity matrix
np.eye(3)


# In[57]:


#Use NumPy to generate a random number between 0 and 1
np.random.rand(1)


# In[58]:


#Use NumPy to generate an array of 25 random numbers sampled from a standard normal distribution
np.random.randn(25)


# In[59]:


#Create the following matrix
np.linspace(0,1,101)


# In[60]:


#Create an array of 20 linearly spaced points between 0 and 1
np.linspace(0,1,20)


# In[61]:


#Creating mat array
mat = np.arange(1,26).reshape(5,5)
mat


# In[62]:


#Showing the requested result 
mat[2:,1:]


# In[63]:


#Showing the requested result 
mat[3][4]


# In[64]:


#Showing the requested result 
mat[:3,1:2]


# In[65]:


#Showing the requested result 
mat[4]


# In[66]:


#Showing the requested result 
mat[3:,:]


# In[67]:


# Get the sum of all the values in mat
mat.sum()


# In[37]:


#Get the standard deviation of the values in mat
mat.std()


# In[39]:


#Get the sum of all the columns in mat
np.sum(mat,0)

